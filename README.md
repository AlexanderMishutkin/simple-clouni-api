# Clouni Rest API

Simple HTTP Rest API for [Clouni orchestrator](https://github.com/ispras/clouni). Actually, just a sketch project.

Used in [Alien4Cloud Clouni plugin](https://gitlab.com/AlexanderMishutkin/a4c-clouni-plugin).

## Main issues

* <b color="red">Probably unsave shell argument inserting!</d>
* Unsychronized CSAR generation and downloading - two people can not use API at the same time.
