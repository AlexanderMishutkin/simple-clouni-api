from flask import Flask, request, jsonify
import subprocess

app = Flask(__name__)


@app.route('/', methods=['GET'])
def run():
    args = request.args

    with open('topology.yaml', 'w+') as f:
        f.write(args.get('topology'))

    result = subprocess.run(
        [
            "clouni",
            "--template-file",
            "topology.yaml",
            "--cluster-name",
            args.get('cluster-name'),
            "--provider",
            args.get('provider')
        ], capture_output=True, text=True
    )

    return jsonify({"stdout": result.stdout, "stderr": result.stderr})


if __name__ == '__main__':
    app.debug = True
    app.run(host="0.0.0.0", port=50051)
