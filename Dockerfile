FROM python:3.6.13-slim
LABEL maintainer="ISP RAS"
WORKDIR /app/
RUN apt update &&\
    apt install git -y
RUN git clone https://github.com/bura2017/tosca-parser.git &&\
    cd tosca-parser &&\
    git checkout develop &&\
    pip install -U -r requirements.txt &&\
    python setup.py install
RUN git clone https://github.com/ispras/clouni.git &&\
    cd clouni &&\
    git checkout grpc &&\
    pip install -U -r requirements.txt &&\
    pip install -U -r requirements-grpc.txt &&\
    python setup.py install
RUN git clone https://gitlab.com/AlexanderMishutkin/simple-clouni-api.git &&\
    cd simple-clouni-api &&\
    pip install flask
RUN apt remove git -y &&\
    apt autoremove -y &&\
    apt clean
EXPOSE 50051
CMD python main.py
